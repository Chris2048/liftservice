/**
 * 
 */
package com.java.fairqueue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import com.java.fairqueue.FairQueue;

import static org.hamcrest.Matchers.empty;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import junit.framework.TestCase;
import static org.mockito.Mockito.*;

/**
 * @author CHancock
 * 
 * 
 */
public class FairQueueTest extends TestCase {

	FairQueue<Object> testSubject;

	protected void setUp() throws Exception {
		testSubject = new FairQueue<Object>(5);
	}

	protected void tearDown() throws Exception {
		testSubject = null;
	}
	
	class PausingConsumer implements Runnable {
		private FairQueue<Object> queueToPoll;
		private List<Object> orders;
		
		public PausingConsumer(FairQueue<Object> queueToPoll, List<Object> orders){
			this.queueToPoll = queueToPoll;
			this.orders = orders;
		}

		public void run() {
			Object item = queueToPoll.poll();
			this.orders.add(item);
		}	
	}

	/**
	 * Test orderings for for {@link com.java.fairqueue.FairQueue}.
	 */
	public void testOrdering() {
		List<Object> orders = Collections.synchronizedList(new ArrayList<Object>());

		PausingConsumer firstThread = new PausingConsumer(testSubject, orders);
		PausingConsumer secondThread = new PausingConsumer(testSubject, orders);
		PausingConsumer thirdThread = new PausingConsumer(testSubject, orders);
		
		Object first = mock(Object.class);
		Object second = mock(Object.class);
		Object third = mock(Object.class);
		
		firstThread.run();
		secondThread.run();
		thirdThread.run();
		
		testSubject.offer(first);
		testSubject.offer(second);
		testSubject.offer(third);
		
		assertThat(orders.remove(0), is(first));
		assertThat(orders.remove(0), is(second));
		assertThat(orders.remove(0), is(third));
	}

}








