package com.java.liftservice;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/userPanel")
public class UserPanelServlet extends HttpServlet {
	private static final String CONTROL_LIFT_NAME = "MyLift";
	private static final long serialVersionUID = -4678179507744837751L;
	
	PanelManager panelManager = PanelManager.getPanelManager(CONTROL_LIFT_NAME);
	
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		int floor;
		try {
			floor = Integer.valueOf(request.getParameter("floor"));
		} catch (NumberFormatException e) {
			response.sendRedirect("/liftservice/");
			return;
		}
		
		if (floor < 0 || floor > panelManager.getNumberOfFloors()){
			response.sendRedirect("/liftservice/");
			return;
		}

		request.setAttribute("panelInfo", panelManager.getPanelInfo(floor));
		System.out.println(panelManager.getPanelInfo(floor).getRequestedDown());
		request.getRequestDispatcher("/WEB-INF/user.jsp").forward(request,
				response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String floor = request.getParameter("floor");
		String button = request.getParameter("liftRequest");
		if ("requestUp".equals(button)) {
			System.out.println("requested to go up");
			panelManager.requestUp(Integer.valueOf(floor));
		} else if ("requestDown".equals(button)) {
			System.out.println("requested to go down");
			panelManager.requestDown(Integer.valueOf(floor));
		}

		response.sendRedirect("/liftservice/userPanel?floor=" + floor);
	}

}