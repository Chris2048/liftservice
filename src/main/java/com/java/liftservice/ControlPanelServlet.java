package com.java.liftservice;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/controlPanel")
public class ControlPanelServlet extends HttpServlet {
	private static final String CONTROL_LIFT_NAME = "MyLift";
	private static final long serialVersionUID = -4845287633860791364L;
	
	PanelManager panelManager = PanelManager.getPanelManager(CONTROL_LIFT_NAME);

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("panelInfos", panelManager.getUpdatedPanelInfos());
		request.getRequestDispatcher("/WEB-INF/control.jsp").forward(request, response);
	}
	
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String floor = request.getParameter("floor");
		String button = request.getParameter("liftRequest");
		if ("requestCome".equals(button)) {
			System.out.println("request to go to floor " + floor);
			panelManager.requestCome(Integer.valueOf(floor));
		}

		response.sendRedirect("/liftservice/controlPanel");
	}
}