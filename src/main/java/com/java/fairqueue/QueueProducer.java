package com.java.fairqueue;

public class QueueProducer implements Runnable {
	private final FairQueue<String> queue;
	private int count;


	public QueueProducer(FairQueue<String> queue) {
		this.queue = queue;
	}

	public void produce() {
		new Thread(this).start();
	}

	public void run() {
		int count;
		synchronized (this) {
			count = this.count++;
		}
		
		String item = String.valueOf(count);
		
		try {
			queue.put(item);
		} catch (InterruptedException e) {
			System.out.println("Couldn't produce, exception thrown!");
			e.printStackTrace();
		}
	}
}
