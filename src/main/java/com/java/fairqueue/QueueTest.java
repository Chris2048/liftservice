package com.java.fairqueue;

public class QueueTest {
	FairQueue<String> fairQueue = new FairQueue<String>(20);

	public static void main(String[] args) {
		new QueueTest().startTest();
	}

	private void startTest() {

		new Thread(new Runnable() {

			public void run() {
				for (int i = 1000; i > 0; --i) {
					new QueueConsumer(fairQueue, String.valueOf(i)).consume();
				}
				System.out.println("Consumers finished.");
			}

		}).start();

		new Thread(new Runnable() {

			public void run() {
				for (int i = 1000; i > 0; --i) {
					new QueueProducer(fairQueue).produce();
				}
				System.out.println("Producers finished.");
			}

		}).start();
	}
}
