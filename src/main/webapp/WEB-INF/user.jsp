<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<!DOCTYPE html>
<html lang="en">
<head>
<title>User Panel</title>
<script
	src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
</head>
<body>
	This is a user panel, for floor ${panelInfo.getFloorName()}

	<br />
	<div id="flashBox" style="display: none"></div>
	<br />

	<c:choose>
		<c:when test="${!panelInfo.getRequestedUp()}">
			<form id="upForm" name="submitForm" method="POST" action="/liftservice/userPanel">
				<input type="hidden" name="liftRequest" value="requestUp">
				<input type="hidden" name="floor" value="${panelInfo.getFloorNumber()}">
				<div onClick="document.getElementById('upForm').submit()">request to go up</div>
			</form>
		</c:when>
		<c:otherwise>
			<div
				onClick="$('#flashBox').text('Please don\'t bash the buttons\!').fadeIn(400).delay(2000).fadeOut(800)">
				requested to go up</div>
		</c:otherwise>
	</c:choose>

	<c:choose>
		<c:when test="${!panelInfo.getRequestedDown()}">
			<form id="downForm" name="submitDownForm" method="POST" action="/liftservice/userPanel">
				<input type="hidden" name="liftRequest" value="requestDown">
				<input type="hidden" name="floor" value="${panelInfo.getFloorNumber()}">
				<div onClick="document.getElementById('downForm').submit()">request to go down</div>
			</form>
		</c:when>
		<c:otherwise>
			<div onClick="$('#flashBox').text('Please don\'t bash the buttons\!').fadeIn(400).delay(2000).fadeOut(800)">requested to go down</div>
		</c:otherwise>
	</c:choose>

	<br /> ${panelInfo.getRequestedCome() ? "<br />Additionally, there has been a request to come to this floor." : ""}
</body>
</html>